package endpoint

import (
	"context"
	"license-control/control/pkg/grpc/pb"
	service "license-control/control/pkg/service"

	endpoint "github.com/go-kit/kit/endpoint"
)

// ApiRequest collects the request parameters for the Api method.
type ApiRequest struct {
	Req service.Payload `json:"data"`
}

// ApiResponse collects the response parameters for the Api method.
type ApiResponse struct {
	Res service.Payload `json:"data"`
	Err error           `json:"err"`
}

// MakeApiEndpoint returns an endpoint that invokes Api on the service.
func MakeApiEndpoint(s service.ControlService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ApiRequest)
		res, err := s.Api(ctx, req.Req)
		return ApiResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r ApiResponse) Failed() error {
	return r.Err
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// Api implements Service. Primarily useful in a client.
func (e Endpoints) Api(ctx context.Context, req service.Payload) (res service.Payload, err error) {
	request := ApiRequest{Req: req}
	response, err := e.ApiEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(ApiResponse).Res, response.(ApiResponse).Err
}

// LicenseRequest collects the request parameters for the License method.
type LicenseRequest struct {
	Req *pb.LicenseRequest `json:"req"`
}

// LicenseResponse collects the response parameters for the License method.
type LicenseResponse struct {
	Res *pb.LicenseReply `json:"res"`
	Err error            `json:"err"`
}

// MakeLicenseEndpoint returns an endpoint that invokes License on the service.
func MakeLicenseEndpoint(s service.ControlService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(LicenseRequest)
		res, err := s.License(ctx, req.Req)
		return LicenseResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r LicenseResponse) Failed() error {
	return r.Err
}

// License implements Service. Primarily useful in a client.
func (e Endpoints) License(ctx context.Context, req *pb.LicenseRequest) (res *pb.LicenseReply, err error) {
	request := LicenseRequest{Req: req}
	response, err := e.LicenseEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(LicenseResponse).Res, response.(LicenseResponse).Err
}

// KeepAliveControlRequest collects the request parameters for the KeepAliveControl method.
type KeepAliveControlRequest struct {
	Req *pb.KeepAliveControlRequest `json:"req"`
}

// KeepAliveControlResponse collects the response parameters for the KeepAliveControl method.
type KeepAliveControlResponse struct {
	Res *pb.KeepAliveControlReply `json:"res"`
	Err error                     `json:"err"`
}

// MakeKeepAliveControlEndpoint returns an endpoint that invokes KeepAliveControl on the service.
func MakeKeepAliveControlEndpoint(s service.ControlService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(KeepAliveControlRequest)
		res, err := s.KeepAliveControl(ctx, req.Req)
		return KeepAliveControlResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r KeepAliveControlResponse) Failed() error {
	return r.Err
}

// KeepAliveControl implements Service. Primarily useful in a client.
func (e Endpoints) KeepAliveControl(ctx context.Context, req *pb.KeepAliveControlRequest) (res *pb.KeepAliveControlReply, err error) {
	request := KeepAliveControlRequest{Req: req}
	response, err := e.KeepAliveControlEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(KeepAliveControlResponse).Res, response.(KeepAliveControlResponse).Err
}

// RootRequest collects the request parameters for the Root method.
type RootRequest struct {
	Req *pb.RootRequest `json:"req"`
}

// RootResponse collects the response parameters for the Root method.
type RootResponse struct {
	Res *pb.RootReply `json:"res"`
	Err error         `json:"err"`
}

// MakeRootEndpoint returns an endpoint that invokes Root on the service.
func MakeRootEndpoint(s service.ControlService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(RootRequest)
		res, err := s.Root(ctx, req.Req)
		return RootResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r RootResponse) Failed() error {
	return r.Err
}

// Root implements Service. Primarily useful in a client.
func (e Endpoints) Root(ctx context.Context, req *pb.RootRequest) (res *pb.RootReply, err error) {
	request := RootRequest{Req: req}
	response, err := e.RootEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(RootResponse).Res, response.(RootResponse).Err
}
