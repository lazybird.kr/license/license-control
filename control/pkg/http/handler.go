package http

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"license-control/control/pkg/endpoint"
	http1 "net/http"
	"os"

	"github.com/go-kit/kit/transport/http"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// makeApiHandler creates the handler logic
func makeApiHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/api").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.ApiEndpoint, decodeApiRequest, encodeApiResponse, options...)))
	m.Methods("POST").Path("/api/").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.ApiEndpoint, decodeApiRequest, encodeApiResponse, options...)))
	m.Methods("POST").Path("/api/{category}/{service}").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.ApiEndpoint, decodeApiRequest, encodeApiResponse, options...)))
}

// decodeApiRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeApiRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	vars := mux.Vars(r)
	req := endpoint.ApiRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)

	if len(req.Req.Category) == 0 && len(vars["category"]) > 0 {
		req.Req.Category = vars["category"]
	}
	if len(req.Req.Service) == 0 && len(vars["service"]) > 0 {
		req.Req.Service = vars["service"]
	}

	return req, err
}

// encodeApiResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeApiResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
func ErrorEncoder(_ context.Context, err error, w http1.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}
func ErrorDecoder(r *http1.Response) error {
	var w errorWrapper
	if err := json.NewDecoder(r.Body).Decode(&w); err != nil {
		return err
	}
	return errors.New(w.Error)
}

// This is used to set the http status, see an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/pkg/addtransport/http.go#L133
func err2code(err error) int {
	return http1.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}

// makeLicenseHandler creates the handler logic
func makeLicenseHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/license").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.LicenseEndpoint, decodeLicenseRequest, encodeLicenseResponse, options...)))
}

// decodeLicenseRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeLicenseRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.LicenseRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeLicenseResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeLicenseResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeKeepAliveControlHandler creates the handler logic
func makeKeepAliveControlHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/keep-alive-control").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.KeepAliveControlEndpoint, decodeKeepAliveControlRequest, encodeKeepAliveControlResponse, options...)))
}

// decodeKeepAliveControlRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeKeepAliveControlRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.KeepAliveControlRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeKeepAliveControlResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeKeepAliveControlResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

func NoRouteFun(w http1.ResponseWriter, r *http1.Request) {
	filePath := os.Getenv("LICENSE_FRONTEND_HOME") + r.URL.Path
	//fmt.Fprintf(os.Stderr, "DEBUG: %v\n", filePath)
	if _, err := os.Stat(filePath); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		fmt.Fprintf(w, "404 Not Found Page !! : %v%v", r.Host, r.URL.Path)
		return
	}

	http1.ServeFile(w, r, filePath)
}

// makeRootHandler creates the handler logic
func makeRootHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.NotFoundHandler = http1.HandlerFunc(NoRouteFun)
	fmt.Fprintf(os.Stdout, "RootHander: %v\n", os.Getenv("LICENSE_FRONTEND_HOME"))
	m.Methods("GET").Path("/").Handler(http1.FileServer(http1.Dir(os.Getenv("LICENSE_FRONTEND_HOME"))))
}

// decodeRootRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeRootRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.RootRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeRootResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeRootResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
