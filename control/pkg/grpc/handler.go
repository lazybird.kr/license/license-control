package grpc

import (
	"context"
	"errors"
	"license-control/control/pkg/endpoint"
	"license-control/control/pkg/grpc/pb"

	"github.com/go-kit/kit/transport/grpc"
	context1 "golang.org/x/net/context"
)

func makeApiHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.ApiEndpoint, decodeApiRequest, encodeApiResponse, options...)
}

func decodeApiRequest(_ context.Context, r interface{}) (interface{}, error) {
	return nil, errors.New("'Control' Decoder is not impelemented")

}

func encodeApiResponse(_ context.Context, r interface{}) (interface{}, error) {
	return nil, errors.New("'Control' Encoder is not impelemented")
}
func (g *grpcServer) Api(ctx context1.Context, req *pb.ApiRequest) (*pb.ApiReply, error) {
	_, rep, err := g.api.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.ApiReply), nil
}

func makeLicenseHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.LicenseEndpoint, decodeLicenseRequest, encodeLicenseResponse, options...)
}

func decodeLicenseRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.LicenseRequest)

	return endpoint.LicenseRequest{
		Req: req,
	}, nil
}

func encodeLicenseResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.LicenseResponse)

	return res.Res, res.Failed()
}
func (g *grpcServer) License(ctx context1.Context, req *pb.LicenseRequest) (*pb.LicenseReply, error) {
	_, rep, err := g.license.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.LicenseReply), nil
}

// makeKeepAliveControlHandler creates the handler logic
func makeKeepAliveControlHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.KeepAliveControlEndpoint, decodeKeepAliveControlRequest, encodeKeepAliveControlResponse, options...)
}

// decodeKeepAliveControlResponse is a transport/grpc.DecodeRequestFunc that converts a
// gRPC request to a user-domain KeepAliveControl request.
// TODO implement the decoder
func decodeKeepAliveControlRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.KeepAliveControlRequest)
	return endpoint.KeepAliveControlRequest{Req: req}, nil
}

// encodeKeepAliveControlResponse is a transport/grpc.EncodeResponseFunc that converts
// a user-domain response to a gRPC reply.
// TODO implement the encoder
func encodeKeepAliveControlResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.KeepAliveControlResponse)
	return res.Res, res.Failed()
}
func (g *grpcServer) KeepAliveControl(ctx context1.Context, req *pb.KeepAliveControlRequest) (*pb.KeepAliveControlReply, error) {
	_, rep, err := g.keepAliveControl.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.KeepAliveControlReply), nil
}

// makeRootHandler creates the handler logic
func makeRootHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.RootEndpoint, decodeRootRequest, encodeRootResponse, options...)
}

// decodeRootResponse is a transport/grpc.DecodeRequestFunc that converts a
// gRPC request to a user-domain Root request.
// TODO implement the decoder
func decodeRootRequest(_ context.Context, r interface{}) (interface{}, error) {
	return nil, errors.New("'Control' Decoder is not impelemented")
}

// encodeRootResponse is a transport/grpc.EncodeResponseFunc that converts
// a user-domain response to a gRPC reply.
// TODO implement the encoder
func encodeRootResponse(_ context.Context, r interface{}) (interface{}, error) {
	return nil, errors.New("'Control' Encoder is not impelemented")
}
func (g *grpcServer) Root(ctx context1.Context, req *pb.RootRequest) (*pb.RootReply, error) {
	_, rep, err := g.root.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.RootReply), nil
}
