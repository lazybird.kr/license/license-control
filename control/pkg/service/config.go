package service

import (
	"lazybird.com/license-common/config"
)

var ServerHostConfig config.ServerConfig
var ClientHostConfig config.ClientConfig
var DBConfig config.DBConfig

func init() {
	if err := config.LoadConfig(); err != nil {
		panic(err)
	}

	_ = config.GetConfig()
	ServerHostConfig = config.GetServerHostConfig()
	ClientHostConfig = config.GetClientHostConfig()
	DBConfig = config.GetDBConfig()
}

func GetConfigServerControlGrpc() string {
	return ServerHostConfig.Control.GrpcHosts
}

func GetConfigServerControlHttp() string {
	return ServerHostConfig.Control.HttpHosts
}

func GetConfigClientUsageGrpc() string {
	return ClientHostConfig.Usage.GrpcHosts
}

func GetConfigClientKeyGrpc() string {
	return ClientHostConfig.Key.GrpcHosts
}

func GetConfigClientPolicyGrpc() string {
	return ClientHostConfig.Policy.GrpcHosts
}