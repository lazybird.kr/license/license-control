package service

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type LicenseUser struct {}

func (s *LicenseUser) Service (ctx context.Context, req Payload)(res Payload, err error) {
	switch req.Service {
	case "SignUp":
		res, err = s.SignUp(ctx, req)
	case "GetAccountData":
		res, err = s.GetAccountData(ctx, req)
	case "RegisterEmail":
		res, err = s.RegisterEmail(ctx, req)
	}
	return 
}

func (s *LicenseUser) SignUp (ctx context.Context, req Payload) (res Payload, err error) {
	if req.User == nil {
		err = errors.New("user_info is mandatory")
		return
	}
	if len(req.User.Email) == 0 {
		err = errors.New("user_info.email is mandatory")
		return
	}

	if len(req.User.Pass) == 0 {
		err = errors.New("user_info.password is mandatory")
		return
	}

	var loginObj = LoginObject{
		Account: req.User.Email,
	}
	err = loginObj.ValidateAccount(req.User.Email)
	if err != nil {
		return
	}
	err = loginObj.ValidatePassword(req.User.Pass)
	if err != nil {
		return
	}
	loginObj.EncodePassword(req.User.Email, req.User.Pass)

	var userObj = UserObject{
		Login: loginObj,
	}
	err = userObj.Create()
	if err != nil {
		return
	}

	return
}

func (s *LicenseUser) GetAccountData (ctx context.Context, req Payload) (res Payload, err error) {
	if req.User == nil {
		err = errors.New("user_info is mandatory")
		return
	}

	if len(req.User.Email) == 0 {
		err = errors.New("user_info.email is mandatory")
		return
	}

	var userObj = UserObject{
		Login: LoginObject{
			Account: req.User.Email,
		},
	}

	user, err := userObj.Read()
	if err != nil {
		return
	}

	res.User = &UserInfoObject{
		UserId: user.Id.Hex(),
		Email: user.Login.Account,
		Pass: user.Login.Password,
	}

	return
}

func (s *LicenseUser) RegisterEmail (ctx context.Context, req Payload) (res Payload, err error) {
	if req.User == nil {
		err = errors.New("user_info is mandatory")
		return
	}

	if len(req.User.Email) == 0 {
		err = errors.New("user_info.email is mandatory")
		return
	}

	var loginObj = LoginObject{
		Account: req.User.Email,
	}
	err = loginObj.ValidateAccount(req.User.Email)
	if err != nil {
		return
	}

	var userObj = UserObject{
		Login: loginObj,
		//Status: "NoPassword",
	}

	err = userObj.Create()
	if err != nil {
		return
	}

	res.User = &UserInfoObject{
		UserId: userObj.Id.Hex(),
	}

	return
}

func (s *LicenseUser) ReadUserInfoBulk (ctx context.Context, req Payload)(res Payload, err error) {
	if len(req.UserIdList) == 0 {
		err = errors.New("user_id_list is mandatory")
		return
	}

	var idList []primitive.ObjectID
	for _, id := range req.UserIdList {
		pid, err2 := primitive.ObjectIDFromHex(id)
		if err2 == nil {
			idList = append(idList, pid)
		}
	}

	d := UserObject{}
	uInfos, err := d.ReadBulk(idList)
	if err != nil {
		return
	}

	userMap := make(map[string]UserInfoObject)
	for _, user := range uInfos {
		userMap[user.Id.Hex()] = UserInfoObject{
			UserId: user.Id.Hex(),
			Email: user.Login.Account,
		}
	}

	res.UserMap = userMap

	return
}