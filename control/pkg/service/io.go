package service

type Payload struct {
	Category          string                    `json:"category,omitempty"`
	Service           string                    `json:"service,omitempty"`
	Key               string                    `json:"license_key,omitempty"`
	KeyId             string                    `json:"key_id,omitempty"`
	MacAddress        []string                  `json:"mac_address,omitempty"`
	Registered        bool                      `json:"registered,omitempty"`
	KeyUsage          *KeyUsageObject           `json:"key_usage,omitempty"`
	UsageList         []KeyUsageObject          `json:"usage_list,omitempty"`
	Status            string                    `json:"key_status,omitempty"`
	ValidationInfo    *KeyValidObject           `json:"validation_info,omitempty"`
	ProductName       string                    `json:"product_name,omitempty"`
	StaticKeyInfo     *LicenseKeyObject         `json:"static_info,omitempty"`
	StaticKeyInfoList []LicenseKeyObject        `json:"static_key_info_list,omitempty"`
	KeyList           []string                  `json:"key_list,omitempty"`
	UsageMap          map[string]KeyUsageObject `json:"usage_map,omitempty"`
	KeyListInfo       []KeyInfoObject           `json:"key_info_list,omitempty"`
	KeyInfo           *KeyInfoObject            `json:"key_info,omitempty"`
	KeyMap            map[string]KeyInfoObject  `json:"key_map,omitempty"`
	User              *UserInfoObject           `json:"user_info,omitempty"`
	UserMap           map[string]UserInfoObject `json:"user_map,omitempty"`
	UserIdList        []string                  `json:"user_id_list,omitempty"`
	PolicyType        string                    `json:"policy_type,omitempty"`
	Duration          string                    `json:"duration,omitempty"`
	Policy            *LicensePolicyObject      `json:"policy,omitempty"`
	PolicyList        []LicensePolicyObject     `json:"policy_list,omitempty"`
	PolicyId          string                    `json:"policy_id,omitempty"`
	IdList            []string                  `json:"id_list,omitempty"`
	Count             int64                     `json:"count,omitempty"`
	Name              string                    `json:"name,omitempty"`
}

type LicenseKeyObject struct {
	KeyId    string `json:"-,omitempty"`
	License  string `json:"license_key,omitempty"`
	PolicyId string `json:"policy_id, omitempty"`
}

type KeyUsageObject struct {
	UsageId    string           `json:"usage_id,omitempty"`
	Key        string           `json:"key,omitempty"`
	MacAddress []string         `json:"mac_list,omitempty"`
	UserId     []string         `json:"user_id_list,omitempty"`
	UserInfo   []UserInfoObject `json:"user_list,omitempty"`
	ActiveTime string           `json:"active_time,omitempty"`
	Status     string           `json:"status,omitempty"`
	RemainDay  int64            `json:"remain_day,omitempty"`
}

type KeyValidObject struct {
	Key        string `json:"license_key,omitempty"`
	Status     string `json:"usage_status,omitempty"`
	ActiveTime string `json:"active_time,omitempty"`
	Validation bool   `json:"validation,omitempty"`
	RemainDay  int64  `json:"remain_day,omitempty"`
}

type KeyInfoObject struct {
	KeyId        string               `json:"key_id,omitempty"`
	BasicKeyInfo *LicenseKeyObject    `json:"basic_key_info"`
	KeyUsageInfo *KeyUsageObject      `json:"key_usage"`
	PolicyInfo   *LicensePolicyObject `json:"policy_info"`
}

type UserInfoObject struct {
	UserId string `json:"id,omitempty"`
	Email  string `json:"email,omitempty"`
	Pass   string `json:"password,omitempty"`
}

type LicensePolicyObject struct {
	Id         string `json:"id,omitempty"`
	PolicyType string `json:"policy_type,omitempty"`
	Duration   int64  `json:"duration,omitempty"`
	Name       string `json:"policy_name,omitempty"`
}
