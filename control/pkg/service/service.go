package service

import (
	"context"
	"license-control/control/pkg/grpc/pb"
)

// ControlService describes the service.
type ControlService interface {
	// Add your methods here
	// e.x: Foo(ctx context.Context,s string)(rs string, err error)
	Api(ctx context.Context, req Payload) (res Payload, err error)
	Root(ctx context.Context, req *pb.RootRequest) (res *pb.RootReply, err error)
	License(ctx context.Context, req *pb.LicenseRequest) (res *pb.LicenseReply, err error)
	KeepAliveControl(ctx context.Context, req *pb.KeepAliveControlRequest) (res *pb.KeepAliveControlReply, err error)
}

type basicControlService struct{}

func (b *basicControlService) Api(ctx context.Context, req Payload) (res Payload, err error) {
	switch req.Category {
	case "usage":
		s := LicenseUsage{}
		res, err = s.service(ctx, req)
	case "key":
		s := LicenseKey{}
		res, err = s.Service(ctx, req)
	case "user":
		s := LicenseUser{}
		res, err = s.Service(ctx, req)
	case "policy":
		s := LicensePolicy{}
		res, err = s.Service(ctx, req)
	}

	return res, err
}

// NewBasicControlService returns a naive, stateless implementation of ControlService.
func NewBasicControlService() ControlService {
	return &basicControlService{}
}

// New returns a ControlService with all of the expected middleware wired in.
func New(middleware []Middleware) ControlService {
	var svc ControlService = NewBasicControlService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicControlService) License(ctx context.Context, req *pb.LicenseRequest) (res *pb.LicenseReply, err error) {
	d := LicenseAuth{
		License:    req.LicenseKey,
		MacAddress: req.MacAddress,
	}

	if req.User != nil {
		d.Email = req.User.Email
	}

	switch req.Service {
	case "CheckLicenseOfMac":
		err = d.CheckLicenseOfMac(ctx)
	case "RegisterKeyWithMac":
		err = d.RegisterKeyWithMac(ctx)
	}

	res = &pb.LicenseReply{
		KeyValidation: &pb.LicenseReply_KeyValidObject{
			LicenseKey: d.License,
			Status:     d.Status,
			ActiveTime: d.ActiveTime,
			Validation: d.Validation,
			RemainDay:  d.RemainDay,
		},
		Registered: d.Registered,
	}

	return res, err
}

func (b *basicControlService) KeepAliveControl(ctx context.Context, req *pb.KeepAliveControlRequest) (res *pb.KeepAliveControlReply, err error) {
	res = &pb.KeepAliveControlReply{
		Ack: req.Syn,
	}
	return res, err
}

func (b *basicControlService) Root(ctx context.Context, req *pb.RootRequest) (res *pb.RootReply, err error) {
	// TODO implement the business logic of Root
	return res, err
}
