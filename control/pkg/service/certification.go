package service

import (
	"errors"
	"fmt"
	"lazybird.com/license-common/config"
	"math"
	"os"
	"time"
)

func GetValidationKey (req Payload) (res Payload, err error) {
	if len(req.KeyMap) == 0 {
		err = errors.New("key_map is mandatory")
		return
	}

	validation := KeyValidObject{
		RemainDay: 0,
	}

	for _, key := range req.KeyMap {
		usage := key.KeyUsageInfo
		policy := key.PolicyInfo
		if usage.Status == "activate" {
			fmt.Fprintf(os.Stdout, "policy:%v - %v\n", policy.Name, policy.PolicyType)
			if policy.PolicyType == "subscription" {
				activeTime, _ := time.Parse(config.GetTimeFormat(), usage.ActiveTime)
				since := time.Since(activeTime)
				duration := math.Round(since.Hours() / 24)
				if int64(duration) <= policy.Duration {
					remain := policy.Duration - int64(duration)
					if validation.RemainDay < remain {
						validation.Key = key.BasicKeyInfo.License
						validation.Status = usage.Status
						validation.RemainDay = remain
						validation.ActiveTime = usage.ActiveTime
						validation.Validation = true
					}
				}
			} else {
				validation.Key = key.BasicKeyInfo.License
				validation.ActiveTime = usage.ActiveTime
				validation.Status = usage.Status
				validation.RemainDay = 9999
				validation.Validation = true
			}
			fmt.Fprintf(os.Stdout, "GetValidationKey: %v\n", validation.RemainDay)
		}
	}

	res.ValidationInfo = &validation
	return
}

func (keyInfo *KeyInfoObject)CalcRemainDay () () {
	if keyInfo.KeyUsageInfo == nil {
		return
	}
	if keyInfo.PolicyInfo == nil {
		return
	}

	usage := keyInfo.KeyUsageInfo
	policy := keyInfo.PolicyInfo
	if usage.Status == "activate" {
		if policy.PolicyType == "subscription" {
			activeTime, _ := time.Parse(config.GetTimeFormat(), usage.ActiveTime)
			since := time.Since(activeTime)
			elapsed := math.Round(since.Hours() / 24)
			if int64(elapsed) < policy.Duration {
				keyInfo.KeyUsageInfo.RemainDay = policy.Duration - int64(elapsed)
			} else {
				keyInfo.KeyUsageInfo.RemainDay = 0
			}
		} else {
			keyInfo.KeyUsageInfo.RemainDay = 9999
		}
	}

	return
}