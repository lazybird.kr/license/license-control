package service

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"gopkg.in/mgo.v2/bson"
	"os"
)

type UserObject struct {
	Id           primitive.ObjectID `bson:"_id,omitempty"`
	Name         string             `bson:"name,omitempty"`
	Nickname     string             `bson:"nickname,omitempty"`
	Login        LoginObject        `bson:"login,omitempty"`
	SecretToken  SecretTokenObject  `bson:"secret_token,omitempty"`
	Client       ClientObject       `bson:"client,omitempty"`
	Status       string             `bson:"status,omitempty"`
	Time         TimeLogObject      `bson:"time,omitempty"`
}

func (d *UserObject) Create() (err error) {
	collection := GetCollection()

	_, err = collection.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bsonx.Doc{{"login.account", bsonx.Int32(1)}},
			Options: options.Index().SetUnique(true),
		})
	if err != nil {
		err = fmt.Errorf("collection.Index.CreateOne: %v", err)
		return
	}

	fmt.Fprintf(os.Stdout, "%v account\n", d.Login.Account)

	d.Time.Initialize()

	result, err := collection.InsertOne(context.TODO(), d)
	if err != nil {
		err = fmt.Errorf("insert: %v", err)
		return
	}

	d.Id = result.InsertedID.(primitive.ObjectID)

	return
}

func (d *UserObject) Read() (obj UserObject, err error) {
	collection := GetCollection()

	readBson := bson.M{}
	if d.Id.IsZero() == false {
		readBson["_id"] = d.Id
	}
	if len(d.Login.Account) > 0 {
		readBson["login.account"] = d.Login.Account
	}

	err = collection.FindOne(context.Background(), readBson).Decode(&obj)
	if err != nil {
		return
	}
	return
}

func (d *UserObject) ReadBulk(idList []primitive.ObjectID) (objList []UserObject, err error) {
	collection := GetCollection()

	if len(idList) == 0 {
		return
	}

	readBson := bson.M{
		"_id": bson.M{"$in": idList},
	}

	cursor, err := collection.Find(context.Background(), readBson)
	if err != nil {
		return
	}
	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var doc UserObject
		if err := cursor.Decode(&doc); err != nil {
			err = fmt.Errorf("ReadAll:%v\n", err)
		} else {
			objList = append(objList, doc)
		}
	}

	return
}

func (d *UserObject) ReadAll(skip int, limit int) (objList []UserObject, err error) {
	collection := GetCollection()

	readBson := bson.M{}
	if len(d.Login.Account) > 0 {
		readBson["login.account"] = d.Login.Account
	}

	cursor, err := collection.Find(context.Background(), readBson)
	if err != nil {
		return
	}
	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var doc UserObject
		if err := cursor.Decode(&doc); err != nil {
			err = fmt.Errorf("ReadAll:%v\n", err)
		} else {
			objList = append(objList, doc)
		}
	}

	return
}
func (d *UserObject) Update() (err error) {
	collection := GetCollection()

	updateBson := bson.M{}

	if len(d.Login.Password) > 0 {
		updateBson["login.password"] = d.Login.Password
	}
	if len(d.Name) > 0 {
		updateBson["name"] = d.Name
	}
	if len(d.Nickname) > 0 {
		updateBson["nickname"] = d.Nickname
	}
	if len(d.SecretToken.Token) > 0 {
		updateBson["secret_token"] = d.SecretToken
	}
	if len(d.Status) > 0 {
		updateBson["status"] = d.Status
	}
	if d.Time.LoginTime.IsZero() == false {
		updateBson["time.login_time"] = d.Time.LoginTime
	}

	d.Time.Update()
	updateBson["time.update_time"] = d.Time.UpdateTime

	_, err = collection.UpdateOne(context.Background(), bson.M{"_id": d.Id}, bson.M{"$set": updateBson})
	if err != nil {
		return
	}

	return
}

func (d *UserObject) Delete() (err error) {
	collection := GetCollection()

	_, err = collection.DeleteOne(context.Background(), bson.M{"_id": d.Id})
	if err != nil {
		return
	}

	return
}
func (d *UserObject) DeleteDuplicatedAccount() {
	collection := GetCollection()

	_, _ = collection.DeleteOne(context.Background(), bson.M{"login.account": d.Login.Account})
}

func (d *UserObject) Validate() (err error) {
	if len(d.Name) < 1 {
		err = errors.New("'name' is mandatory")
		return
	}
	if len(d.Nickname) == 0 {
		d.Nickname = d.Name
	}

	return
}
