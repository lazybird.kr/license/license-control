package service

import (
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/keepalive"
	"os"
	"time"
)

type GrpcObject struct {
	UsageConn *grpc.ClientConn
	KeyConn   *grpc.ClientConn
	PolicyConn *grpc.ClientConn
}

var GrpcConn GrpcObject

var opts []grpc.DialOption

var kacp = keepalive.ClientParameters{
	Time:                60 * time.Second, // send pings every 60 seconds if there is no activity
	Timeout:             time.Second,      // wait 1 second for ping ack before considering the connection dead
	PermitWithoutStream: true,             // send pings even without active streams
}

func init() {
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithKeepaliveParams(kacp))
	go func() {
		for range time.Tick(time.Second * 60) {
			GrpcConn.ConnectionCheck()
		}
	}()

	return
}

func (g *GrpcObject) ConnectionCheck() {
	{
		conn, _ := g.GetUsageConn()
		if conn != nil {
			d := LicenseUsage{}
			_, err := d.KeepAlive(nil, Payload{})
			if err != nil {
				//fmt.Fprintf(os.Stderr, "DEBUG: UsageConn, This message is printed when rpc connection is wrong: %v\n", g.UsageConn.GetState())
				g.UsageConn.Close()
				g.UsageConn = nil
			}
		}
	}
	{
		conn, _ := g.GetKeyConn()
		if conn != nil {
			d := LicenseKey{}
			_, err := d.KeepAlive(nil, Payload{})
			if err != nil {
				//fmt.Fprintf(os.Stderr, "DEBUG: KeyConn, This message is printed when rpc connection is wrong: %v\n", g.KeyConn.GetState())
				g.KeyConn.Close()
				g.KeyConn = nil
			}
		}
	}
	{
		conn, _ := g.GetPolicyConn()
		if conn != nil {
			d := LicensePolicy{}
			_, err := d.KeepAlive(nil, Payload{})
			if err != nil {
				g.PolicyConn.Close()
				g.PolicyConn = nil
			}
		}
	}
}

func (g *GrpcObject) GetUsageConn() (conn *grpc.ClientConn, err error) {
	if g.UsageConn != nil {
		if g.UsageConn.GetState() != connectivity.Ready && g.UsageConn.GetState() != connectivity.Idle {
			fmt.Fprintf(os.Stderr, "UsageConn - Warning !! %v\n", g.UsageConn.GetState())
			g.UsageConn = nil
		}
	}

	if g.UsageConn == nil {
		addr := GetConfigClientUsageGrpc()
		g.UsageConn, err = grpc.Dial(addr, opts...)
		if err != nil {
			err = fmt.Errorf("fail to dial: %v", err)
			return
		}
	}

	conn = g.UsageConn

	return
}

func (g *GrpcObject) GetKeyConn() (conn *grpc.ClientConn, err error) {
	if g.KeyConn != nil {
		if g.KeyConn.GetState() != connectivity.Ready && g.KeyConn.GetState() != connectivity.Idle {
			fmt.Fprintf(os.Stderr, "KeyConn - Warning !! %v\n", g.KeyConn.GetState())
			g.KeyConn = nil
		}
	}

	if g.KeyConn == nil {
		addr := GetConfigClientKeyGrpc()
		g.KeyConn, err = grpc.Dial(addr, opts...)
		if err != nil {
			err = fmt.Errorf("fail to Dial: %v\n", err)
			return
		}
	}

	conn = g.KeyConn
	return
}

func (g *GrpcObject) GetPolicyConn() (conn *grpc.ClientConn, err error) {
	if g.PolicyConn != nil {
		if g.PolicyConn.GetState() != connectivity.Ready && g.PolicyConn.GetState() != connectivity.Idle {
			g.PolicyConn = nil
		}
	}

	if g.PolicyConn == nil {
		addr := GetConfigClientPolicyGrpc()
		g.PolicyConn, err = grpc.Dial(addr, opts...)
		if err != nil {
			err = fmt.Errorf("fail to Dial: %v\n", err)
			return
		}
	}

	conn = g.PolicyConn

	return
}