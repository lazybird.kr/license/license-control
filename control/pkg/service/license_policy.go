package service

import (
	"context"
	"errors"
	"fmt"
	"lazybird.com/license-policy/policy/pkg/grpc/pb"
	"strconv"
)

type LicensePolicy struct{}

func (s *LicensePolicy) Service(ctx context.Context, req Payload) (res Payload, err error) {
	switch req.Service {
	case "KeepAlive":
		res, err = s.KeepAlive(ctx, req)
	case "CreatePolicy":
		res, err = s.CreatePolicy(ctx, req)
	case "ReadPolicyBulk":
		res, err = s.ReadPolicyBulk(ctx, req)
	case "ReadPolicyAll":
		res, err = s.ReadPolicyAll(ctx, req)
	}

	return
}

func (s *LicensePolicy) KeepAlive(ctx context.Context, req Payload) (res Payload, err error) {
	message := pb.KeepAlivePolicyRequest{
		Ping: "hello",
	}

	conn, err := GrpcConn.GetPolicyConn()
	if err != nil {
		return
	}

	c := pb.NewPolicyClient(conn)
	_, err2 := c.KeepAlivePolicy(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.KeepAlivePolicyRequest: %v\n", err2)
		return
	}

	return
}

func (s *LicensePolicy) CreatePolicy(ctx context.Context, req Payload) (res Payload, err error) {
	duration, err := strconv.ParseInt(req.Duration, 10, 64)
	message := pb.CreatePolicyRequest{
		Type:     req.PolicyType,
		Duration: duration,
		Name:     req.Name,
	}

	conn, err := GrpcConn.GetPolicyConn()
	if err != nil {
		return
	}

	c := pb.NewPolicyClient(conn)
	reply, err := c.CreatePolicy(
		context.Background(),
		&message,
	)
	if err != nil {
		return
	}

	res.Policy = &LicensePolicyObject{
		Id: reply.Id,
	}

	return
}

func (s *LicensePolicy) ReadPolicyBulk(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.IdList) == 0 {
		err = errors.New("id_list is mandatory")
		return
	}

	message := pb.ReadPolicyBulkRequest{
		IdList: req.IdList,
	}

	conn, err := GrpcConn.GetPolicyConn()
	if err != nil {
		return
	}

	c := pb.NewPolicyClient(conn)
	reply, err := c.ReadPolicyBulk(
		context.Background(),
		&message,
	)
	if err != nil {
		return
	}

	var policyList []LicensePolicyObject
	for _, policy := range reply.PolicyList {
		policyList = append(policyList, LicensePolicyObject{
			Id:         policy.Id,
			PolicyType: policy.Type,
			Duration:   policy.Duration,
			Name:       policy.Name,
		})
	}

	res.PolicyList = policyList
	return
}

func (s *LicensePolicy) ReadPolicyAll(ctx context.Context, req Payload) (res Payload, err error) {
	message := pb.ReadAllPolicyRequest{}

	conn, err := GrpcConn.GetPolicyConn()
	if err != nil {
		return
	}

	c := pb.NewPolicyClient(conn)
	reply, err := c.ReadAllPolicy(
		context.Background(),
		&message,
	)

	var policyList []LicensePolicyObject
	for _, policy := range reply.PolicyList {
		policyList = append(policyList, LicensePolicyObject{
			Id:         policy.Id,
			PolicyType: policy.Type,
			Duration:   policy.Duration,
			Name:       policy.Name,
		})
	}

	res.PolicyList = policyList

	return
}
