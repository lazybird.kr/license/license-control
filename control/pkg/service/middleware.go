package service

import (
	"context"
	"license-control/control/pkg/grpc/pb"

	log "github.com/go-kit/kit/log"
)

type Middleware func(ControlService) ControlService

type loggingMiddleware struct {
	logger log.Logger
	next   ControlService
}

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next ControlService) ControlService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) Api(ctx context.Context, req Payload) (res Payload, err error) {
	defer func() {
		l.logger.Log("method", "Api", "req", req, "res", res, "err", err)
	}()
	return l.next.Api(ctx, req)
}

func (l loggingMiddleware) License(ctx context.Context, req *pb.LicenseRequest) (res *pb.LicenseReply, err error) {
	defer func() {
		l.logger.Log("method", "License", "req", req, "res", res, "err", err)
	}()
	return l.next.License(ctx, req)
}

func (l loggingMiddleware) KeepAliveControl(ctx context.Context, req *pb.KeepAliveControlRequest) (res *pb.KeepAliveControlReply, err error) {
	defer func() {
		l.logger.Log("method", "KeepAliveControl", "req", req, "res", res, "err", err)
	}()
	return l.next.KeepAliveControl(ctx, req)
}

func (l loggingMiddleware) Root(ctx context.Context, req *pb.RootRequest) (res *pb.RootReply, err error) {
	defer func() {
		l.logger.Log("method", "Root", "req", req, "res", res, "err", err)
	}()
	return l.next.Root(ctx, req)
}
