package service

import (
	"context"
	"errors"
	"fmt"
	"lazybird.com/license-key/key/pkg/grpc/pb"
)

type LicenseKey struct{}

func (s *LicenseKey) Service(ctx context.Context, req Payload) (res Payload, err error) {
	switch req.Service {
	case "KeepAlive":
		res, err = s.KeepAlive(ctx, req)
	case "CreateLicenseKey":
		res, err = s.CreateLicenseKey(ctx, req)
	case "ReadLicenseAll":
		res, err = s.ReadLicenseAll(ctx, req)
	case "ReadLicenseKeyInfo":
		res, err = s.ReadLicenseKeyInfo(ctx, req)
	case "ReadLicenseKey":
		res, err = s.ReadLicenseKey(ctx, req)
	}

	return
}

func (s *LicenseKey) KeepAlive(ctx context.Context, req Payload) (res Payload, err error) {
	message := pb.KeepAliveKeyRequest{
		Ping: "hello",
	}
	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewKeyClient(conn)
	_, err2 := c.KeepAliveKey(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.KeepAliveKeyRequest: %v", err2)
		return
	}

	return
}

func (s *LicenseKey) CreateLicenseKey(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.ProductName) == 0 {
		// TODO : change product id
		err = errors.New("product_name is mandatory")
		return
	}
	if len(req.PolicyId) == 0 {
		err = errors.New("policy_id is mandatory")
		return
	}

	fmt.Println("CreateLicenseKey")

	message := pb.CreateLicenseKeyRequest{
		ProductId: req.ProductName,
		PolicyId:  req.PolicyId,
	}

	conn, err := GrpcConn.GetKeyConn()
	if err != nil {
		return
	}

	c := pb.NewKeyClient(conn)
	reply, err2 := c.CreateLicenseKey(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.CreateLicenseKey: %v\n", err2)
		return
	}

	res.Key = reply.KeyId

	obj := LicenseUsage{}
	_, _ = obj.CreateKeyUsage(ctx, res)

	res.Key = reply.License

	return
}

func (s *LicenseKey) ReadLicenseKey(ctx context.Context, req Payload) (res Payload, err error) {
	//fmt.Fprintf(os.Stdout, "ReadLicenseKey:%v\n", req.Key)
	if len(req.Key) == 0 {
		err = errors.New("license_key is mandatory")
		return
	}

	message := pb.ReadLicenseKeyRequest{
		LicenseKey: req.Key,
	}

	conn, err := GrpcConn.GetKeyConn()
	if err != nil {
		return
	}

	c := pb.NewKeyClient(conn)
	reply, err2 := c.ReadLicenseKey(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.ReadLicenseKey: %v\n", err2)
		return
	}

	res.StaticKeyInfo = &LicenseKeyObject{
		KeyId:    reply.KeyId,
		License:  reply.LicenseKey,
		PolicyId: reply.PolicyId,
	}

	return
}

func (s *LicenseKey) ReadLicenseAll(ctx context.Context, req Payload) (res Payload, err error) {
	message := pb.ReadLicenseKeyAllRequest{}

	conn, err := GrpcConn.GetKeyConn()
	if err != nil {
		return
	}

	c := pb.NewKeyClient(conn)
	reply, err2 := c.ReadLicenseKeyAll(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("ReadLicenseKeyAll: %v\n", err2)
		return
	}

	var keyIdList []string
	var keyInfoList [] LicenseKeyObject
	var policyIdList []string
	for _, key := range reply.KeyList {
		keyIdList = append(keyIdList, key.KeyId)
		policyIdList = append(policyIdList, key.PolicyId)
		keyInfoList = append(keyInfoList, LicenseKeyObject{
			KeyId:    key.KeyId,
			License:  key.LicenseKey,
			PolicyId: key.PolicyId,
		})
	}

	policyReq := Payload{
		IdList: policyIdList,
	}
	policyObj := LicensePolicy{}
	policyRes, policyErr := policyObj.ReadPolicyBulk(ctx, policyReq)
	if policyErr != nil {
		err = policyErr
		return
	}
	policyMap := make(map[string]*LicensePolicyObject)
	for i, policy := range policyRes.PolicyList {
		policyMap[policy.Id] = &policyRes.PolicyList[i]
	}

	req.KeyList = keyIdList
	obj := LicenseUsage{}
	usageRes, err2 := obj.GetKeyUsageByKeyList(ctx, req)
	if err2 != nil {
		err = err2
		return
	}

	usages := usageRes.UsageMap
	var userIdList []string
	for _, usage := range usages {
		userIdList = append(userIdList, usage.UserId...)
	}

	userReq := Payload{
		UserIdList: userIdList,
	}
	userObj := LicenseUser{}
	userRes, _ := userObj.ReadUserInfoBulk(ctx, userReq)
	usageUserMap := make(map[string]UserInfoObject)
	for _, usage := range usages {
		if len(usage.UserId) > 0 {
			userId := usage.UserId[0]
			usageUserMap[usage.Key] = userRes.UserMap[userId]
		}

	}

	var totalInfo []KeyInfoObject
	for _, key := range keyInfoList {
		usage := usages[key.KeyId]
		basic := key
		usage.UserInfo = append(usage.UserInfo, usageUserMap[usage.Key])
		totalInfo = append(totalInfo, KeyInfoObject{
			KeyId:        key.KeyId,
			BasicKeyInfo: &basic,
			KeyUsageInfo: &usage,
			PolicyInfo:   policyMap[key.PolicyId],
		})
	}

	var expiredList []string
	for _, info := range totalInfo {
		info.CalcRemainDay()
		if info.KeyUsageInfo.Status == "activate" {
			if info.KeyUsageInfo.RemainDay == 0 {
				info.KeyUsageInfo.Status = "expired"
				expiredList = append(expiredList, info.KeyUsageInfo.UsageId)
			}
		}
	}

	usageReq := Payload{
		IdList: expiredList,
	}
	_, _ = obj.ExpireUsageBulk(ctx, usageReq)

	res.KeyListInfo = totalInfo

	return
}

func (s *LicenseKey) ReadLicenseKeyInfo(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.Key) == 0 {
		err = errors.New("license_key is mandatory")
		return
	}

	keyRes, err := s.ReadLicenseKey(ctx, req)
	if err != nil {
		return
	}

	req2 := Payload{
		KeyId: keyRes.StaticKeyInfo.KeyId,
	}
	obj := LicenseUsage{}
	usageRes, err := obj.GetKeyUsage(ctx, req2)
	if err != nil {
		return
	}

	res.KeyInfo = &KeyInfoObject{
		KeyId: keyRes.StaticKeyInfo.KeyId,
		BasicKeyInfo: &LicenseKeyObject{
			License: keyRes.StaticKeyInfo.License,
		},
		KeyUsageInfo: &KeyUsageObject{
			UsageId:    usageRes.KeyUsage.UsageId,
			Status:     usageRes.KeyUsage.Status,
			ActiveTime: usageRes.KeyUsage.ActiveTime,
			UserId:     usageRes.KeyUsage.UserId,
			MacAddress: usageRes.KeyUsage.MacAddress,
		},
	}

	return
}

func (s *LicenseKey) ReadLicenseKeyById(ctx context.Context, req Payload) (res Payload, err error) {
	if req.StaticKeyInfo == nil {
		err = errors.New("static_info is mandatory")
		return
	}
	if len(req.StaticKeyInfo.KeyId) == 0 {
		err = errors.New("key_id is mandatory")
		return
	}

	message := pb.ReadLicenseByIdRequest{
		KeyId: req.StaticKeyInfo.KeyId,
	}

	conn, err := GrpcConn.GetKeyConn()
	if err != nil {
		return
	}

	c := pb.NewKeyClient(conn)
	reply, err := c.ReadLicenseById(
		context.Background(),
		&message,
	)
	if err != nil {
		return
	}

	res.StaticKeyInfo = &LicenseKeyObject{
		KeyId:   reply.KeyId,
		License: reply.LicenseKey,
	}

	return
}

func (s *LicenseKey) ReadLicenseKeyBulkById(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.KeyList) == 0 {
		err = errors.New("key_list is mandatory")
		return
	}

	message := pb.ReadLicenseBulkByIdRequest{
		KeyIdList: req.KeyList,
	}

	conn, err := GrpcConn.GetKeyConn()
	if err != nil {
		return
	}

	c := pb.NewKeyClient(conn)
	reply, err := c.ReadLicenseBulkById(
		context.Background(),
		&message,
	)
	if err != nil {
		return
	}

	var keyList []LicenseKeyObject
	for _, key := range reply.KeyList {
		keyList = append(keyList, LicenseKeyObject{
			KeyId:    key.KeyId,
			License:  key.LicenseKey,
			PolicyId: key.PolicyId,
		})
	}

	res.StaticKeyInfoList = keyList

	return
}
