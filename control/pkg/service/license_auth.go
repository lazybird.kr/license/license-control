package service

import (
	"context"
	"errors"
	"fmt"
	"lazybird.com/license-common/config"
	"strings"
	"time"
)

type LicenseAuth struct {
	KeyId      string
	License    string
	Status     string
	ActiveTime string
	Validation bool
	MacAddress []string
	Registered bool
	Email      string
	RemainDay  int64
}

func (d *LicenseAuth) CheckLicenseOfMac(ctx context.Context) (err error) {
	d.Validation = true
	d.RemainDay = 9999
	return

	//fmt.Fprintf(os.Stdout, "CheckLicenseOfMac: %v\n", d.MacAddress)
	usageReq := Payload{
		MacAddress: d.MacAddress,
	}

	usageObj := LicenseUsage{}
	usageRes, err := usageObj.GetActivateKeyOfMac(ctx, usageReq)
	if err != nil {
		return
	}

	if len(usageRes.UsageList) == 0 {
		d.Validation = false
		return
	}

	var keyList []string
	usages := make(map[string]*KeyUsageObject)
	for _, usage := range usageRes.UsageList {
		usages[usage.Key] = &KeyUsageObject{
			UsageId:    usage.UsageId,
			MacAddress: usage.MacAddress,
			ActiveTime: usage.ActiveTime,
			Status:     usage.Status,
		}

		keyList = append(keyList, usage.Key)
	}

	keyObj := LicenseKey{}
	keyReq := Payload{
		KeyList: keyList,
	}
	keyRes, err2 := keyObj.ReadLicenseKeyBulkById(ctx, keyReq)
	if err2 != nil {
		err = err2
		return
	}

	if len(keyRes.StaticKeyInfoList) == 0 {
		//fmt.Fprintf(os.Stdout, "no key list %v\n", keyList)
		err = errors.New("no key list")
		return
	}

	var policyIdList []string
	keyMap := make(map[string]KeyInfoObject)
	policyKeyMap := make(map[string]string)
	for _, key := range keyRes.StaticKeyInfoList {
		policyIdList = append(policyIdList, key.PolicyId)
		policyKeyMap[key.PolicyId] = key.KeyId
		keyMap[key.KeyId] = KeyInfoObject{
			KeyId: key.KeyId,
			BasicKeyInfo: &LicenseKeyObject{
				License: key.License,
			},
			KeyUsageInfo: usages[key.KeyId],
			PolicyInfo: &LicensePolicyObject{
				Id: key.PolicyId,
			},
		}
	}

	policyReq := Payload{
		IdList: policyIdList,
	}
	policyObj := LicensePolicy{}
	policyRes, policyErr := policyObj.ReadPolicyBulk(ctx, policyReq)
	if policyErr != nil {
		err = policyErr
		return
	}

	for _, policy := range policyRes.PolicyList {
		keyId := policyKeyMap[policy.Id]
		keyMap[keyId].PolicyInfo.Duration = policy.Duration
		keyMap[keyId].PolicyInfo.PolicyType = policy.PolicyType
	}

	validReq := Payload{
		KeyMap: keyMap,
	}
	result, err := GetValidationKey(validReq)
	if err != nil {
		return
	}

	d.License = result.ValidationInfo.Key
	d.ActiveTime = result.ValidationInfo.ActiveTime
	d.Status = result.ValidationInfo.Status
	d.Validation = result.ValidationInfo.Validation
	d.RemainDay = result.ValidationInfo.RemainDay



	return
}

func (d *LicenseAuth) RegisterKeyWithMac(ctx context.Context) (err error) {

	//userObj := LicenseUser{}
	//userRes, err := userObj.GetAccountData(ctx, req)
	//if err != nil {
	//	if strings.Contains(err.Error(), "no documents") {
	//		userRes, err = userObj.RegisterEmail(ctx, req)
	//	} else {
	//		return
	//	}
	//}
	if len(d.License) == 0 {
		err = errors.New("license is mandatory")
		return
	}
	if len(d.MacAddress) == 0 {
		err = errors.New("mac_address is mandatory")
		return
	}

	keyReq := Payload{
		Key: d.License,
	}
	keyObj := LicenseKey{}
	keyRes, err := keyObj.ReadLicenseKey(ctx, keyReq)
	if err != nil {
		err = fmt.Errorf("invalid license key: %v", d.License)
		d.Validation = false
		return
	}

	d.KeyId = keyRes.StaticKeyInfo.KeyId

	usageReq := Payload{
		KeyId: d.KeyId,
	}
	obj := LicenseUsage{}
	usageRes, err2 := obj.GetKeyUsage(ctx, usageReq)
	if err2 != nil {
		if strings.Contains(err2.Error(), "no documents") {
			fmt.Println("no documents error")
			d.Status = "noUsage"
		} else {
			err = err2
			return
		}
	} else {
		checkMac := true
		for i := 0; i < len(usageRes.KeyUsage.MacAddress); i ++ {
			if usageRes.KeyUsage.MacAddress[i] != d.MacAddress[i] {
				checkMac = false
				break
			}
		}
		if checkMac {
			d.Registered = true
			return
		} else {
			d.Status = usageRes.KeyUsage.Status
			d.ActiveTime = usageRes.KeyUsage.ActiveTime
		}
	}

	if d.Status == "noUsage" || d.Status == "standby" {
		activeTime := time.Now().UTC()
		newUsage := KeyUsageObject{
			Key:        keyRes.StaticKeyInfo.KeyId,
			MacAddress: d.MacAddress,
			ActiveTime: activeTime.Format(config.GetTimeFormat()),
			Status:     "activate",
		}
		if len(d.Email) > 0 {
			userObj := LicenseUser{}
			userReq := Payload{
				User: &UserInfoObject{
					Email: d.Email,
				},
			}

			userRes, userErr := userObj.RegisterEmail(ctx, userReq)
			if userErr != nil {
				return
			} else {
				usageReq.User = &UserInfoObject{
					UserId: userRes.User.UserId,
				}
			}
		}

		usageReq.KeyUsage = &newUsage
		_, err3 := obj.UpdateUsageWithKeyId(ctx, usageReq)
		if err3 != nil {
			err = err3
			return
		}

		d.Registered = true
	} else {
		err = errors.New("invalid license key")
		d.Registered = false
	}

	return
}
