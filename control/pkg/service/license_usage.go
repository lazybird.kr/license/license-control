package service

import (
	"context"
	"errors"
	"fmt"
	"lazybird.com/license-usage/usage/pkg/grpc/pb"
)

type LicenseUsage struct{}

func (s *LicenseUsage) service(ctx context.Context, req Payload) (res Payload, err error) {
	switch req.Service {
	case "KeepAlive":
		res, err = s.KeepAlive(ctx, req)
	case "CheckRegisteredAddress":
		res, err = s.CheckRegisteredAddress(ctx, req)
	case "GetKeyUsage":
		res, err = s.GetKeyUsage(ctx, req)
	case "CreateKeyUsage":
		res, err = s.CreateKeyUsage(ctx, req)
	case "GetKeyUsageByKeyList":
		res, err = s.GetKeyUsageByKeyList(ctx, req)
	}
	return
}

func (s *LicenseUsage) KeepAlive(ctx context.Context, req Payload) (res Payload, err error) {
	message := pb.KeepAliveUsageRequest{
		Ping: "hello",
	}
	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewUsageClient(conn)
	_, err2 := c.KeepAliveUsage(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.KeepAliveUsageRequest: %v", err2)
		return
	}

	return
}

func (s *LicenseUsage) CheckRegisteredAddress(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.MacAddress) == 0 {
		err = errors.New("mac_address is mandatory")
		return
	}

	message := pb.ReadKeyUsageWithMacRequest{
		MacAddress: req.MacAddress,
	}

	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewUsageClient(conn)
	keyUsages, err2 := c.ReadKeyUsageWithMac(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.ReadKeyUsageWithMac: %v\n", err2)
		return
	}

	if keyUsages.Count > 0 {
		res.Registered = true
	} else {
		res.Registered = false
	}

	return
}

func (s *LicenseUsage) GetKeyUsage(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.KeyId) == 0 {
		err = errors.New("key_id is mandatory")
		return
	}

	message := &pb.ReadKeyUsageInfoRequest{
		Key: req.KeyId,
	}

	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewUsageClient(conn)
	reply, err2 := c.ReadKeyUsageInfo(
		context.Background(),
		message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.GetKeyUsage: %v\n", err2)
		return
	}

	usage := KeyUsageObject{
		UsageId:    reply.Id,
		Key:        reply.Key,
		MacAddress: reply.MacAddress,
		UserId:     reply.UserId,
		ActiveTime: reply.ActiveTime,
		Status:     reply.Status,
	}

	res.KeyUsage = &usage

	return
}

func (s *LicenseUsage) CreateKeyUsage(ctx context.Context, req Payload) (res Payload, err error) {
	message := pb.CreateKeyUsageInfoRequest{
		Key: req.Key,
	}

	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewUsageClient(conn)
	_, err2 := c.CreateKeyUsageInfo(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("CreateKeyUsageInfo: %v\n", err2)
		return
	}

	return
}

func (s *LicenseUsage) GetKeyUsageByKeyList(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.KeyList) == 0 {
		err = errors.New("key_list is mandatory")
		return
	}

	message := pb.ReadKeyUsageBulkRequest{
		KeyList: req.KeyList,
	}

	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewUsageClient(conn)
	reply, err2 := c.ReadKeyUsageBulk(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = err2
		return
	}

	usageList := make(map[string]KeyUsageObject)
	for _, usage := range reply.KeyUsages {
		usageList[usage.Key] = KeyUsageObject{
			UsageId:    usage.Id,
			Key:        usage.Key,
			MacAddress: usage.MacAddress,
			UserId:     usage.UserId,
			ActiveTime: usage.ActiveTime,
			Status:     usage.Status,
		}
	}

	res.UsageMap = usageList

	return
}

func (s *LicenseUsage) GetActivateKeyOfMac(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.MacAddress) == 0 {
		err = errors.New("mac_address is mandatory")
		return
	}

	message := pb.ReadKeyUsageWithMacRequest{
		MacAddress: req.MacAddress,
	}

	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewUsageClient(conn)
	reply, err2 := c.ReadKeyUsageWithMac(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.ReadKeyUsageWithMac: %v\n", err2)
		return
	}

	var usages []KeyUsageObject
	for _, usage := range reply.KeyUsages {
		if usage.Status == "activate" {
			usages = append(usages, KeyUsageObject{
				UsageId:    usage.Id,
				Key:        usage.Key,
				MacAddress: usage.MacAddress,
				UserId:     usage.UserId,
				ActiveTime: usage.ActiveTime,
				Status:     usage.Status,
			})
		}
	}

	res.UsageList = usages

	return
}

func (s *LicenseUsage) UpdateUsageWithKeyId(ctx context.Context, req Payload) (res Payload, err error) {
	if req.KeyUsage == nil {
		err = errors.New("key_usage is mandatory")
		return
	}
	if len(req.KeyUsage.Key) == 0 {
		err = errors.New("key_id is mandatory")
		return
	}

	message := pb.UpdateKeyUsageInfoRequest{
		Key:        req.KeyUsage.Key,
		ActiveTime: req.KeyUsage.ActiveTime,
		Status:     req.KeyUsage.Status,
	}
	if len(req.KeyUsage.MacAddress) > 0 {
		message.MacAddress = req.KeyUsage.MacAddress[0]
	}

	if req.User != nil && len(req.User.UserId) > 0 {
		message.UserId = req.User.UserId
	}

	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewUsageClient(conn)
	_, err2 := c.UpdateKeyUsageInfo(
		context.Background(),
		&message,
	)
	if err2 != nil {
		err = fmt.Errorf("pb.UpdateKeyUsageInfo: ;%v\n", err2)
		return
	}
	return
}

func (s *LicenseUsage) ExpireUsageBulk(ctx context.Context, req Payload) (res Payload, err error) {
	if len(req.IdList) == 0 {
		err = errors.New("ExpireUsageBulk: id_list is mandatory")
		return
	}

	message := pb.ExpireUsageBulkRequest{
		IdList: req.IdList,
	}

	conn, err := GrpcConn.GetUsageConn()
	if err != nil {
		return
	}

	c := pb.NewUsageClient(conn)
	reply, err := c.ExpireUsageBulk(
		context.Background(),
		&message,
	)
	if err != nil {
		return
	}

	res.Count = reply.Count

	return
}
