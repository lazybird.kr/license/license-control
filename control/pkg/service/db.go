package service

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2"
	"log"
	"time"
)

var mgoSession *mgo.Session

func init() {
	err := initializeMongo()
	if err != nil {
		panic(err)
	}
}

var mongoClient *mongo.Client

func initializeMongo() (err error) {
	credential := options.Credential{
		Username: DBConfig.Username,
		Password: DBConfig.Password,
	}

	clientOptions := options.Client().ApplyURI("mongodb://"+ DBConfig.Hosts)
	if len(DBConfig.Username) > 0 {
		clientOptions = clientOptions.SetAuth(credential)
	}
	clientOptions = clientOptions.SetConnectTimeout(60 * time.Second)
	mongoClient, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	return
}

func GetCollection ()(coll *mongo.Collection) {
	return mongoClient.Database(DBConfig.Database).Collection(DBConfig.UserCollection)
}