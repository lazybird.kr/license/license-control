FROM golang:latest as build
ENV LAZYBIRD_LICENSE /go/src
ENV GO111MODULE=on
ENV CGO_ENABLED=0

WORKDIR /go/src
RUN git clone https://wonsuck_song:wssong98@gitlab.com/lazybird.kr/license/license-common.git
RUN git clone https://wonsuck_song:wssong98@gitlab.com/lazybird.kr/license/license-usage.git
RUN git clone https://wonsuck_song:wssong98@gitlab.com/lazybird.kr/license/license-key.git
RUN git clone https://wonsuck_song:wssong98@gitlab.com/lazybird.kr/license/license-policy.git
WORKDIR /go/src/license-control
COPY go.mod .
COPY go.sum .
RUN go mod download

ADD . .
RUN go build -o app_license_control license-control/control/cmd

FROM node:10.14 as frontend_build
WORKDIR /usr/src
RUN git clone https://wonsuck_song:wssong98@gitlab.com/lazybird.kr/license/license-frontend.git app
WORKDIR /usr/src/app
RUN echo "\nNODE_ENV=production\n" >> .env
RUN yarn install --network-timeout 1000000
ENV PATH /usr/src/app/node_modules/.bin:$PATH
RUN yarn build

FROM alpine:3.9
ENV LAZYBIRD_LICENSE /var/local
ENV LICENSE_FRONTEND_HOME frontend
WORKDIR /var/local/license-common
COPY --from=build /go/src/license-common/docker_config.json ./config.json
WORKDIR /usr/local/bin/frontend
COPY --from=frontend_build /usr/src/app/build .
WORKDIR /usr/local/bin
COPY --from=build /go/src/license-control/app_license_control /usr/local/bin/app_license_control

ENTRYPOINT ["app_license_control"]