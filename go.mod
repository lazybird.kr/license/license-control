module license-control

go 1.13

require (
	github.com/go-kit/kit v0.9.0
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/lightstep/lightstep-tracer-go v0.20.0
	github.com/oklog/oklog v0.3.2
	github.com/opentracing/opentracing-go v1.1.0
	github.com/openzipkin/zipkin-go-opentracing v0.3.5
	github.com/prometheus/client_golang v1.3.0
	go.mongodb.org/mongo-driver v1.3.3
	golang.org/x/net v0.0.0-20200421231249-e086a090c8fd
	google.golang.org/grpc v1.26.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	lazybird.com/license-common v0.0.0
	lazybird.com/license-key v0.0.0
	lazybird.com/license-policy v0.0.0
	lazybird.com/license-usage v0.0.0
	sourcegraph.com/sourcegraph/appdash v0.0.0-20190731080439-ebfcffb1b5c0
)

replace lazybird.com/license-common => ../license-common

replace lazybird.com/license-usage => ../license-usage

replace lazybird.com/license-key => ../license-key

replace lazybird.com/license-policy => ../license-policy
